using System;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace netcore_dapper.api.controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<TasksController> _Logger;
        private readonly IConfiguration _Configuration;

        public HomeController(ILogger<TasksController> logger, IConfiguration configuration)
        {
            this._Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return new RedirectResult($"~{_Configuration["PATH_BASE"]}/swagger");
        }
    }
}