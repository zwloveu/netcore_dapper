# db_prepare.py
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Database Prepare
---------------

prepare sqlite3 db
"""

import sqlite3
from typing import List, Tuple


def create_table(dbpath: str, table_create_sql: str) -> None:
    """Create specified table in specified sqlite3 db

    Parameters:
    -----------
        `dbpath`: 
            str, the sqlite3 db file path
        `table_create_sql`: 
            str, the sql script used to create a specifed table
    Returns:
    --------
    """

    connection = connect_to_sqlite3(dbpath)

    connection.execute(table_create_sql)

    connection.close()


def connect_to_sqlite3(dbpth: str) -> sqlite3.Connection:
    """connect to sqlite3 db

    Parameters:
    -----------
        `dbpath`: 
            str, the sqlite3 db file path
    Returns:
    --------
        The sqlite3.Connection instance
    """

    return sqlite3.connect(dbpth)


def query(dbpth: str, query_sql: str) -> List[Tuple]:
    """query from sqlite3 db

    Parameters:
    ----------
        `dbpath`: 
            str, the sqlite3 db file path
        `query_sql`: 
            str, the sql to query data
    Returns:
    --------
        A List[Tuple] data fetched from db, if no return empty list []
    """

    result: List[Tuple] = []

    connection = connect_to_sqlite3(dbpth)

    for row in connection.execute(query_sql):
        result.append(row)

    connection.close()

    return result


def executemany(dbpath: str, execute_sql: str, data: List[Tuple]) -> None:
    """execute the sql with data

    Parameters:
    -----------
        `dbpath`: 
            str, the sqlite3 db file path
        `execute_sql`: 
            str, sql that to be executed
        `data`: 
            List[Tuple], data which the execution sql needs when execute
    Returns:
    -------
    """

    connection = connect_to_sqlite3(dbpath)

    connection.executemany(execute_sql, data)

    connection.commit()

    connection.close()


def query(dbpth: str, query_sql: str) -> List[Tuple]:
    """query from sqlite3 db

    Parameters:
    -----------
        `dbpath`: 
            str, the sqlite3 db file path
        `query_sql`: 
            str, the sql to query data
    Returns:
    --------
        A List[Tuple] data fetched from db, if no return empty list []
    """

    result: list = []

    connection = connect_to_sqlite3(dbpth)

    for row in connection.execute(query_sql):
        result.append(row)

    connection.close()

    return result


CREATE_TABLE_TASKS_DB_SCRIPT: str = """
CREATE TABLE IF NOT EXISTS Tasks(
    [Id]                        INTEGER PRIMARY KEY AUTOINCREMENT,
    [Name]                      NVARCHAR (100) NOT NULL,
    [Description]               NVARCHAR (500) NOT NULL,
    [Status]                    INTEGER NOT NULL,
    [Duedate]                   DATETIME DEFAULT (datetime('now', 'localtime')),
    [Datecreated]               DATETIME DEFAULT (datetime('now', 'localtime')),
    [Datemodified]              DATETIME NULL
);
"""

INSERT_INTO_TABLE_TASKS: str = """
INSERT INTO Tasks (`Name`,`Description`,`Status`)
VALUES (?,?,?);
"""

SELECT_FROM_TABLE_TASKS: str = """
SELECT 
    `Id`,`Name`,`Description`,`Status`,`Duedate`,`Datecreated`,`Datemodified`
FROM Tasks;
"""

DB_FILE: str = "./app.db"

if __name__ == "__main__":
    create_table(DB_FILE, CREATE_TABLE_TASKS_DB_SCRIPT)

    executemany(DB_FILE, INSERT_INTO_TABLE_TASKS, [
                ('testtask', 'this is a test task', 2)])

    result = query(DB_FILE, SELECT_FROM_TABLE_TASKS)

    for i in range(len(result)):
        print(f"row : {i} = {result[i]}")
