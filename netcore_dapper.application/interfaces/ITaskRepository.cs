﻿using netcore_dapper.core.entities;

namespace netcore_dapper.application.interfaces
{
    public interface ITaskRepository: IGenericRepository<Task>
    {
    }
}