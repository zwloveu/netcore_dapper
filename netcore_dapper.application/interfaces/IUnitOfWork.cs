﻿namespace netcore_dapper.application.interfaces
{
    public interface IUnitOfWork
    {
        ITaskRepository Tasks { get; }
    }
}