﻿using MediatR;
using System;
using netcore_dapper.core.enums;

namespace netcore_dapper.application.tasks.commands
{
    public class CreateTaskCommand : IRequest<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskStat Status { get; set; }
        public DateTime DueDate { get; set; }
    }
}
