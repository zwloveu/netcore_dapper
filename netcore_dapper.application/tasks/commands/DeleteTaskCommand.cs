﻿using MediatR;

namespace netcore_dapper.application.tasks.commands
{
    public class DeleteTaskCommand: IRequest<int>
    {
        public int Id { get; set; }
    }
}
