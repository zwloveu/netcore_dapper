﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using netcore_dapper.application.interfaces;
using netcore_dapper.application.tasks.commands;

namespace netcore_dapper.application.tasks.handlers
{
    public class CreateTaskCommandHandler : IRequestHandler<CreateTaskCommand, int>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateTaskCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<int> Handle(CreateTaskCommand request, CancellationToken cancellationToken)
        {
            var request_model = _mapper.Map<netcore_dapper.core.entities.Task>(request);
            var result = await _unitOfWork.Tasks.Add(request_model);
            return result;
        }
    }
}
