﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using netcore_dapper.application.interfaces;
using netcore_dapper.application.tasks.commands;

namespace netcore_dapper.application.tasks.handlers
{
    public class UpdateTaskCommandHandler : IRequestHandler<UpdateTaskCommand, int>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdateTaskCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<int> Handle(UpdateTaskCommand request, CancellationToken cancellationToken)
        {
            var result = await _unitOfWork.Tasks.Update(_mapper.Map<netcore_dapper.core.entities.Task>(request));
            return result;
        }
    }
}
