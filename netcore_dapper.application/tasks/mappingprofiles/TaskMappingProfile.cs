﻿using AutoMapper;
using netcore_dapper.application.tasks.commands;
using netcore_dapper.application.tasks.dto;
using netcore_dapper.core.entities;

namespace netcore_dapper.application.tasks.mappingProfiles
{
    public class TaskMappingProfile: Profile
    {
        public TaskMappingProfile()
        {
            CreateMap<CreateTaskCommand, Task>();
            CreateMap<UpdateTaskCommand, Task>();
            CreateMap<Task, TaskDto>();
        }
    }
}
