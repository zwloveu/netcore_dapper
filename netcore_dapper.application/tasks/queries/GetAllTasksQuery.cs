﻿using MediatR;
using System.Collections.Generic;
using netcore_dapper.application.tasks.dto;

namespace netcore_dapper.application.tasks.queries
{
    public class GetAllTasksQuery: IRequest<List<TaskDto>>
    {
    }
}
