﻿using MediatR;
using netcore_dapper.application.tasks.dto;

namespace netcore_dapper.application.tasks.queries
{
    public class GetTaskByIdQuery: IRequest<TaskDto>
    {
        public int Id { get; set; }
    }
}
