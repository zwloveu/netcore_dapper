using System;
using netcore_dapper.core.enums;

namespace netcore_dapper.core.entities
{
    /// <summary>
    /// Task: represent a task entity
    /// </summary>
    public class Task
    {
        /// <summary>
        /// task id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// task name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// task description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// task status : 0,1,2
        /// </summary>
        public TaskStat Status { get; set; }

        /// <summary>
        /// task due date
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// task created datetime
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// task modified datetime
        /// </summary>
        public DateTime? DateModified { get; set; }
    }
}