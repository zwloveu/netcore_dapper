namespace netcore_dapper.core.enums
{
    /// <summary>
    /// Task Status 
    /// </summary>
    public enum TaskStat
    {
        /// <summary>
        /// Task Status: Created | 0
        /// </summary>
        Created,

        /// <summary>
        /// Task Status: Active | 1
        /// </summary>
        Active,

        /// <summary>
        /// Task Status: Done | 2
        /// </summary>
        Done
    }
}