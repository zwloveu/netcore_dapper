﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using netcore_dapper.db.appdb;

namespace netcore_dapper.db
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddDataContexts(this IServiceCollection services, string conn)
        {            
            services.AddDbContext<AppDbDataContext>(options =>
            {
                options.UseSqlite(conn);
            });

            return services;
        }
    }
}
