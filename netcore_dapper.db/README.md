# Db Migration
```
dotnet ef migrations add AppConfigurationMigration -p netcore_dapper.db.csproj -s ../netcore_dapper.api/netcore_dapper.api.csproj -c AppDbDataContext -o migrations/appdb
```

# Db Script
```
dotnet ef migrations script -p netcore_dapper.db.csproj -s ../netcore_dapper.api/netcore_dapper.api.csproj -c AppDbDataContext  -o migrations/appdb/appdb.sql  
```