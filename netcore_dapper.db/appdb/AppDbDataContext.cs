using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace netcore_dapper.db.appdb
{
    public partial class AppDbDataContext : DbContext
    {
        public AppDbDataContext()
        {
        }

        public AppDbDataContext(DbContextOptions<AppDbDataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Tasks> Tasks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tasks>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("PRIMARY");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnType("INTEGER");

                entity.Property(e => e.Name)
                    .HasColumnType("NVARCHAR(100)");

                entity.Property(e => e.Description)
                    .HasColumnType("NVARCHAR(500)");

                entity.Property(e => e.Status)
                    .HasColumnType("INTEGER");

                entity.Property(e => e.DueDate)
                    .HasColumnType("DATETIME")
                    .HasDefaultValueSql("datetime('now', 'localtime')");

                entity.Property(e => e.DateCreated)
                    .HasColumnType("DATETIME")
                    .HasDefaultValueSql("datetime('now', 'localtime')");

                entity.Property(e => e.DateModified)
                    .HasColumnType("DATETIME");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
