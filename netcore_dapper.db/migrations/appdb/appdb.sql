﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" TEXT NOT NULL CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY,
    "ProductVersion" TEXT NOT NULL
);

CREATE TABLE "Tasks" (
    "Id" INTEGER NOT NULL CONSTRAINT "PRIMARY" PRIMARY KEY AUTOINCREMENT,
    "Name" NVARCHAR(100) NULL,
    "Description" NVARCHAR(500) NULL,
    "Status" INTEGER NOT NULL,
    "DueDate" DATETIME NOT NULL DEFAULT (datetime('now', 'localtime')),
    "DateCreated" DATETIME NOT NULL DEFAULT (datetime('now', 'localtime')),
    "DateModified" DATETIME NULL
);

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20201015013730_AppConfigurationMigration', '3.1.9');

