﻿using Microsoft.Extensions.DependencyInjection;
using netcore_dapper.application.interfaces;
using netcore_dapper.infrastructure.repositories;

namespace netcore_dapper.infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddScoped<ITaskRepository, TaskRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            return services;
        }
    }
}
