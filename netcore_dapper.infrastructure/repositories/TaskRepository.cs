﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;
using System.Linq;
using System.Threading.Tasks;
using netcore_dapper.application.interfaces;

namespace netcore_dapper.infrastructure.repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _conn;

        public TaskRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _conn = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<int> Add(netcore_dapper.core.entities.Task entity)
        {
            entity.DateCreated = DateTime.Now;
            var sql = "INSERT INTO Tasks (Name, Description, Status, DueDate, DateCreated) Values (@Name, @Description, @Status, @DueDate, @DateCreated);";
            using (var connection = new SqliteConnection(_conn))
            {
                connection.Open();
                var affectedRows = await connection.ExecuteAsync(sql, entity);
                return affectedRows;
            }
        }

        public async Task<int> Delete(int id)
        {
            var sql = "DELETE FROM Tasks WHERE Id = @Id;";
            using (var connection = new SqliteConnection(_conn))
            {
                connection.Open();
                var affectedRows = await connection.ExecuteAsync(sql, new { Id = id });
                return affectedRows;
            }
        }

        public async Task<netcore_dapper.core.entities.Task> Get(int id)
        {
            var sql = "SELECT * FROM Tasks WHERE Id = @Id;";
            using (var connection = new SqliteConnection(_conn))
            {
                connection.Open();
                var result = await connection.QueryAsync<netcore_dapper.core.entities.Task>(sql, new { Id = id });
                return result.FirstOrDefault();
            }
        }

        public async Task<IEnumerable<netcore_dapper.core.entities.Task>> GetAll()
        {
            var sql = "SELECT * FROM Tasks;";
            using (var connection = new SqliteConnection(_conn))
            {
                connection.Open();
                var result = await connection.QueryAsync<netcore_dapper.core.entities.Task>(sql);
                return result;
            }
        }

        public async Task<int> Update(netcore_dapper.core.entities.Task entity)
        {
            entity.DateModified = DateTime.Now;
            var sql = "UPDATE Tasks SET Name = @Name, Description = @Description, Status = @Status, DueDate = @DueDate, DateModified = @DateModified WHERE Id = @Id;";
            using (var connection = new SqliteConnection(_conn))
            {
                connection.Open();
                var affectedRows = await connection.ExecuteAsync(sql, entity);
                return affectedRows;
            }
        }
    }
}